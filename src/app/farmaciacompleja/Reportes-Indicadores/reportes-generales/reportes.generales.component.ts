import { Component, OnInit, ViewChild, forwardRef } from '@angular/core';
import { MatTableDataSource, MatPaginator, MatSort, DateAdapter, MAT_DATE_LOCALE, MAT_DATE_FORMATS, MatIconRegistry, MatPaginatorIntl, MatDialog } from '@angular/material';
import { MomentDateAdapter } from '@angular/material-moment-adapter';
import { MY_FORMATS_AUNA, MENSAJES } from 'src/app/common';
import { Meses } from 'src/app/dto/reporte/Meses';
import { Anho } from 'src/app/dto/reporte/Anho';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { DomSanitizer } from '@angular/platform-browser';
import { MatPaginatorIntlEspanol } from 'src/app/directives/matpaginator-translate';
import { CoreService } from 'src/app/service/core.service';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { ReportesGeneralesService } from 'src/app/service/Reportes/reportes-generales.service';
import { ReporteIndicadoresRequest } from 'src/app/dto/configuracion/ReporteIndicadoresRequest';
import { WsResponse } from 'src/app/dto/WsResponse';
import { MessageComponent } from 'src/app/core/message/message.component';
import { ArchivoFTP } from 'src/app/dto/bandeja-preliminar/detalle-preliminar/ArchivoFTP';
import { GenerarReportesGeneralesMoniRequests } from 'src/app/dto/configuracion/GenerarReportesGeneralesMoniRequets';
import { generarReportesGeneralesAutRequest } from 'src/app/dto/configuracion/generarReportesGeneralesAutRequest';

export interface ReporteG {
  id: number;
  reportes: string;
  nombreFile: string;
}

@Component({
  selector: 'app-reportes-generales',
  templateUrl: './reportes.generales.component.html',
  styleUrls: ['./reportes.generales.component.scss'],
  providers: [
    {
      provide: DateAdapter,
      useClass: MomentDateAdapter,
      deps: [MAT_DATE_LOCALE]
    },
    {
      provide: MAT_DATE_FORMATS,
      useValue: MY_FORMATS_AUNA
    },
    {
      provide: MatPaginatorIntl,
      useClass: forwardRef(() => MatPaginatorIntlEspanol)
    }
  ]
})
export class RepoGeneralesComponent implements OnInit {
  mensaje: String;
  verReportes: boolean;
  verReportesAut: boolean;
  isVerReportes: boolean;
  cmbMeses: Meses[];
  cmbAnhos: Anho[];
  fecha: String;
  year: Number;
  month: Number;

  reporteAutorizFrmGrp = new FormGroup({
    fechaInicioAutoriz: new FormControl(null, [Validators.required]),
    fechaFinAutoriz: new FormControl(null, [Validators.required])
  });

  reporteMonitFrmGrp = new FormGroup({
    fechaInicioMonit: new FormControl(null, [Validators.required]),
    fechaFinMonit: new FormControl(null, [Validators.required])
  });

  get fechaInicioAutoriz() { return this.reporteAutorizFrmGrp.get('fechaInicioAutoriz'); }
  get fechaFinAutoriz() { return this.reporteAutorizFrmGrp.get('fechaFinAutoriz'); }
  get fechaInicioMonit() { return this.reporteMonitFrmGrp.get('fechaInicioMonit'); }
  get fechaFinMonit() { return this.reporteMonitFrmGrp.get('fechaFinMonit'); }

  // TABLA INDICADORES
  dataSourceReporteG: MatTableDataSource<ReporteG>;
  dataSourceReporteAutorizador: MatTableDataSource<ReporteG>;
  displayColumnsReporteG = ['reportes', 'verExcel'];
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  reportes: ReporteG[];
  reportesAutorizador: ReporteG[];

  constructor(
    private adapter: DateAdapter<any>,
    private coreService: CoreService,
    private iconRegistry: MatIconRegistry,
    private sanitizer: DomSanitizer,
    private repoGeneralesService: ReportesGeneralesService,
    private dialog: MatDialog,
    private spinnerService: Ng4LoadingSpinnerService
  ) {
    this.adapter.setLocale('es-PE');
    this.reportes = require('src/assets/data/reportes/reportesGenerales.json');
    this.reportesAutorizador = require('src/assets/data/reportes/reporteSolicitudAutorizador.json');
    this.dataSourceReporteG = new MatTableDataSource(this.reportes);
    this.dataSourceReporteAutorizador = new MatTableDataSource(this.reportesAutorizador);
  }

  ngOnInit() {
    this.inicializarVariables();
    this.verReportes = true;
    this.verReportesAut = true;
    this.fecha = '';
    this.iconRegistry.addSvgIcon(
      'excel-icon',
      this.sanitizer.bypassSecurityTrustResourceUrl('./assets/img/icon-excel-2.svg')
    );
  }

  public inicializarVariables(): void {
    this.cmbMeses = require('src/assets/data/constantes/meses.json');
    this.cmbAnhos = require('src/assets/data/constantes/anios.json');
    this.dataSourceReporteG.paginator = this.paginator;
    this.dataSourceReporteG.sort = this.sort;
  }

  /*public mostrarFechaTabla(): void {
    let monthName: string;
    this.year = this.anhosPreFrmCtrl.value;
    this.month = this.mesesPreFrmCtrl.value;
    this.cmbMeses.forEach((elemnt: Meses) => {
      if (elemnt.IdMes === this.month) {
        monthName = elemnt.Mes;
        return;
      }
    });

    let mes = (this.month == null || this.month === undefined) ? '' : monthName;
    let anio = (this.year == null || this.year === undefined) ? '' : this.year + '';
    this.fecha = ' - ' + mes + ' ' + anio;
  }

  public validarFormulario(): boolean {
    if (this.reporteAutorizFrmGrp.invalid) {
      this.anhosPreFrmCtrl.markAsTouched();
      this.mesesPreFrmCtrl.markAsTouched();
      this.openDialogMensaje(MENSAJES.ERROR_CAMPOS, null, true, false, null);
      return false;
    }

    let anioActual: number = new Date().getFullYear();
    let mesActual: number = new Date().getMonth() + 1;
    if (anioActual === this.anhosPreFrmCtrl.value && mesActual < this.mesesPreFrmCtrl.value) {
      this.mensaje = 'El Mes seleccionado es mayor al actual.';
      this.mesesPreFrmCtrl.setValue(null);
      this.mesesPreFrmCtrl.markAsTouched();
      this.openDialogMensaje(MENSAJES.ERROR_CAMPOS, this.mensaje, true, false, null);
      return false;
    }

    return true;
  }*/

  public visualizarReportesPreCargados(): void {
    let reporteRequest = new GenerarReportesGeneralesMoniRequests();
    // reporteRequest.ano = null;
    // reporteRequest.mes = null;
    // reporteRequest.tipo = null;
    reporteRequest.fecIni = this.fechaInicioMonit.value;
    reporteRequest.fecFin = this.fechaFinMonit.value;

    

    this.repoGeneralesService.generarReportesGeneralesMoni(reporteRequest).subscribe((resp) => {
      
      
      if(resp.audiResponse.codigoRespuesta === "0"){
        this.verReportes = false;
        this.mensaje = 'Se genero el filtro exitosamente.';
        this.openDialogMensajeMoni(this.mensaje, null, true, false, null);
        this.spinnerService.hide();
        // this.repoGeneralesService.generarReporteMonitoreo(reporteRequest).subscribe((result) => {
        //   
        // },error => {
        //   
        // })
      }
    }, (err) => {
      //this.isVerReportes = true;
      
    })
  }

  public generarReportesGeneralesAut(reporte: ReporteG): void {
    let reporteRequest = new generarReportesGeneralesAutRequest();
    reporteRequest.fecIni = this.fechaInicioAutoriz.value;
    reporteRequest.fecFin = this.fechaFinAutoriz.value;
    //reporteRequest.ano = this.anhosPreFrmCtrl.value;
    //reporteRequest.mes = this.mesesPreFrmCtrl.value;
    

    this.spinnerService.show();
    this.repoGeneralesService.generarReportesGeneralesAut(reporteRequest)
      .subscribe(
        (response) => {
          
          if(response.audiResponse.codigoRespuesta === "0"){
            this.verReportesAut = false;
            this.mensaje = 'Se genero el filtro exitosamente.';
            this.openDialogMensajeAuto(this.mensaje, null, true, false, null);
            this.spinnerService.hide();
            // this.repoGeneralesService.generarReporteMonitoreo(reporteRequest).subscribe((result) => {
            //   
            // },error => {
            //   
            // })
          }else{
            this.spinnerService.hide();
          }
        },
        error => {
          console.error(error);
          this.mensaje = 'Error en el filtro ' + reporte.reportes;
          this.openDialogMensaje(this.mensaje, null, true, false, null);
          this.spinnerService.hide();
        }
      );
  }


  public crearLinkDescarga(blob: Blob, file: ArchivoFTP): void {
    const link = document.createElement('a');
    link.target = '_blank';
    link.href = window.URL.createObjectURL(blob);
    link.setAttribute('download', file.nomArchivo);
    link.click();
  }

  public verReporteGeneral(reporte: ReporteG): void {
    switch (reporte.id) {
      case 1:
        this.generarReporteSolicitudAutorizaciones(reporte);
        break;
      case 2:
        this.generarReporteMonitoreo(reporte);
        break;
      default:
        break;
    }
  }

  public generarReporteSolicitudAutorizaciones(reporte: ReporteG): void {
    let reporteRequest = new generarReportesGeneralesAutRequest();
    reporteRequest.fecIni = this.fechaInicioAutoriz.value;
    reporteRequest.fecFin = this.fechaFinAutoriz.value;
    //reporteRequest.ano = this.anhosPreFrmCtrl.value;
    //reporteRequest.mes = this.mesesPreFrmCtrl.value;
    

    this.spinnerService.show();
    this.repoGeneralesService.generarReporteSolicitudAutorizaciones(reporteRequest)
      .subscribe(
        (response) => {
          
            let file: ArchivoFTP = new ArchivoFTP();
            file.archivo = response.data;
            file.contentType = 'application/vnd.ms-excel';
            file.nomArchivo = `${reporte.nombreFile}_${(this.fecha.replace(/ - /gi, '').replace(/ /gi, '_'))}.xlsx`;
            this.crearLinkDescarga(this.coreService.crearBlobFile(file), file);
            this.spinnerService.hide();
        },
        error => {
          console.error(error);
          this.mensaje = 'Error al obtener el reporte de ' + reporte.reportes;
          this.openDialogMensaje(this.mensaje, null, true, false, null);
          this.spinnerService.hide();
        }
      );
  }

  public generarReporteMonitoreo(reporte: ReporteG): void {
    let reporteRequest = new GenerarReportesGeneralesMoniRequests();
    reporteRequest.fecIni = this.fechaInicioMonit.value;
    reporteRequest.fecFin = this.fechaFinMonit.value;
    //reporteRequest.ano = this.anhosPreFrmCtrl.value;
    //reporteRequest.mes = this.mesesPreFrmCtrl.value;

    this.spinnerService.show();
    this.repoGeneralesService.generarReporteMonitoreo(reporteRequest)
      .subscribe(
        (response) => {
          
            let file: ArchivoFTP = new ArchivoFTP();
            file.archivo = response.data;
            file.contentType = 'application/vnd.ms-excel';
            file.nomArchivo = `${reporte.nombreFile}_${(this.fecha.replace(/ - /gi, '').replace(/ /gi, '_'))}.xlsx`;
            this.crearLinkDescarga(this.coreService.crearBlobFile(file), file);
            this.spinnerService.hide();
        },
        error => {
          console.error(error);
          this.mensaje = 'Error al obtener el reporte de ' + reporte.reportes;
          this.openDialogMensaje(this.mensaje, null, true, false, null);
          this.spinnerService.hide();
        }
      );
  }

 

  // POP-UP MENSAJES
  public openDialogMensaje(
    message: String,
    message2: String,
    alerta: boolean,
    confirmacion: boolean,
    valor: any): void {
    const dialogRef = this.dialog.open(MessageComponent, {
      disableClose: true,
      width: '400px',
      data: {
        title: 'REPORTES DE CONSUMO',
        message: message,
        message2: message2,
        alerta: alerta,
        confirmacion: confirmacion,
        valor: valor
      }
    });
    dialogRef.afterClosed().subscribe(rspta => {
    });
  }

  public openDialogMensajeAuto(
    message: String,
    message2: String,
    alerta: boolean,
    confirmacion: boolean,
    valor: any): void {
    const dialogRef = this.dialog.open(MessageComponent, {
      disableClose: true,
      width: '400px',
      data: {
        title: 'REPORTES DE SOLICITUDES DE AUTORIZACIONES',
        message: message,
        message2: message2,
        alerta: alerta,
        confirmacion: confirmacion,
        valor: valor
      }
    });
    dialogRef.afterClosed().subscribe(rspta => {
    });
  }

  public openDialogMensajeMoni(
    message: String,
    message2: String,
    alerta: boolean,
    confirmacion: boolean,
    valor: any): void {
    const dialogRef = this.dialog.open(MessageComponent, {
      disableClose: true,
      width: '400px',
      data: {
        title: 'REPORTES GENERAL DE MONITOREO',
        message: message,
        message2: message2,
        alerta: alerta,
        confirmacion: confirmacion,
        valor: valor
      }
    });
    dialogRef.afterClosed().subscribe(rspta => {
    });
  }

}
