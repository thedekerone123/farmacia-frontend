import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/publish';
import { ReporteIndicadoresRequest } from 'src/app/dto/configuracion/ReporteIndicadoresRequest';
import { WsResponse } from 'src/app/dto/WsResponse';
import { Observable } from 'rxjs';
import { webServiceEndpoint } from 'src/app/common';
import { Cookie } from 'ng2-cookies/ng2-cookies';
import { GenerarReportesGeneralesMoniRequests } from 'src/app/dto/configuracion/GenerarReportesGeneralesMoniRequets';
import { reserveSlots } from '@angular/core/src/render3/instructions';
import { generarReportesGeneralesAutRequest } from 'src/app/dto/configuracion/generarReportesGeneralesAutRequest';

@Injectable({
  providedIn: 'root'
})
export class ReportesGeneralesService {

  constructor(private http: HttpClient) { }

  public generarReporteSolicitudAutorizaciones(request: generarReportesGeneralesAutRequest): Observable<WsResponse> {
    return this.http.post<WsResponse>(`${webServiceEndpoint}pub/generarReporteSolicitudAutorizaciones`, request, {
      headers: new HttpHeaders({
        'Content-type': 'application/json; charset=utf-8',
        'authorization': 'Bearer ' + Cookie.get('access_token_fc')
      })
    });
  }
  /**
   * Proposito: Reportes Generales Autorizador
   * @param request
   */
  public generarReportesGenerales(request: ReporteIndicadoresRequest): Observable<WsResponse> {
    return this.http.post<WsResponse>(`${webServiceEndpoint}pub/generarReportesGenerales`, request, {
      headers: new HttpHeaders({
        'Content-type': 'application/json; charset=utf-8',
        'authorization': 'Bearer ' + Cookie.get('access_token_fc')
      })
    });
  }

  /**
   * Proposito: Reportes Generales Monitoreo
   
   */
  public generarReporteMonitoreo(request: GenerarReportesGeneralesMoniRequests): Observable<WsResponse> {
    return this.http.post<WsResponse>(`${webServiceEndpoint}pub/generarReporteMonitoreo`, request, {
      headers: new HttpHeaders({
        'Content-type': 'application/json; charset=utf-8',
        'authorization': 'Bearer ' + Cookie.get('access_token_fc')
      })
    });
  }


  public generarReportesGeneralesAut(request: GenerarReportesGeneralesMoniRequests): Observable<WsResponse> {
    return this.http.post<WsResponse>(`${webServiceEndpoint}pub/generarReportesGeneralesAut`, request, {
      headers: new HttpHeaders({
        'Content-type': 'application/json; charset=utf-8',
        'authorization': 'Bearer ' + Cookie.get('access_token_fc')
      })
    });
  }

  // public generarReporteMonitoreo(request: GenerarReportesGeneralesMoniRequests) {
  //   return this.http.post(`${webServiceEndpoint}pub/generarReporteMonitoreo`, request, {
  //     headers: new HttpHeaders({
  //       'Content-type': 'application/json; charset=utf-8',
  //       'authorization': 'Bearer ' + Cookie.get('access_token_fc'),
  //     }),
  //     responseType: 'blob'
  //   })
  //   .map(
  //     (res) => {
  //       
  //         return new Blob([res], { type: 'application/vnd.ms-excel' });
  //     }
  //   );
  // }


  public generarReportesGeneralesMoni(request: GenerarReportesGeneralesMoniRequests): Observable<WsResponse> {
    return this.http.post<WsResponse>(`${webServiceEndpoint}pub/generarReportesGeneralesMoni`, request, {
      headers: new HttpHeaders({
        'Content-type': 'application/json; charset=utf-8',
        'authorization': 'Bearer ' + Cookie.get('access_token_fc')
      })
    });
  }

}
